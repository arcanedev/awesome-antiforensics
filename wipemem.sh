#!/bin/bash

echo "Wiping your memory before shutdown....";
sleep 10;
sudo sdmem -v;
sleep 5;
sudo shutdown -h now;
exit 1;