apt-get install secure-delete

systemctl disable syslog.service
systemctl disable rsyslog.service
systemctl disable systemd-journald.service

#Purge logging software
apt-get purge rsyslog

#Wipe collected logs
# Note: a limitation of the shred command is that there is no directory traversal; combined with find/xargs and the mission is accomplished
# srm performs 38 passes over the directory structures specified
find /var/log -type f | xargs shred -uvzf -n 32 --remove
find /tmp -type f | xargs shred -uvzf -n 32 --remove
srm -rvz /var/log/*
srm -rvz /tmp/*

# For the respected paranoid - this retraces over old ground
sfill -vz /var/log/
sfill -vz /tmp/